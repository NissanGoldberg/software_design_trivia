An app that allows contestants to improve their knowledge in many areas using trivia question.
There are two types of users.
1) User - uses the trivia to improve his knowledge.
2) Admin - can add new subjects and questions. Also, he may edit previous ones.

There will be statistics per topic according to user and by question.
All the questions and statistics will be held on the DB.